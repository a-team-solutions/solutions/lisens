import { HElements } from "peryl/dist/hsml";
import { HView } from "peryl/dist/hsml-app";
import { h } from "peryl/dist/hsml-h";
import { SidebarState, sidebarState } from "./sidebar";
import { homeView, homeState, HomeState } from "./home";
import { notifState, notifView, NotifState } from "./notif";
import { formState, formView, FormState } from "./form";
import { NavbarState, navbarState, navbarView } from "./navbar";

export interface AppShellState {
    menu: boolean;
    sidebar: SidebarState;
    navbar: NavbarState;
    content: string;
    contents: {
        home: HomeState;
        notif: NotifState;
        form: FormState;
    };
    snackbar: string;
}

export const appShellState: AppShellState = {
    menu: false,
    sidebar: sidebarState,
    navbar: navbarState,
    content: "home",
    contents: {
        home: homeState,
        notif: notifState,
        form: formState
    },
    snackbar: undefined
};

export const enum AppShellAction {
    menu = "appshell-menu",
    mobileMenu = "open-mobile-menu"
}

export const appShellView: HView<AppShellState> = (state): HElements => {
    let content, title;
    switch (state.content) {
        case "notif":
            content = notifView(state.contents.notif);
            title = state.contents.notif.title;
            break;
        case "form":
            content = formView(state.contents.form);
            title = state.contents.form.title;
            break;
        case "home":
        default:
            content = homeView(state.contents.home);
            title = state.contents.home.title;
            break;
    }
    return [
        // Navbar
        navbarView(state.navbar),
        // Title section
        h("section.hero.is-warning", [
            h("div.hero-body", [
                h("div.container", [
                    h("h1.title", title),
                ])
            ])
        ]),
        // main
        h("section.section", [
            h("div.container", [
                h("div.content-wrapper", content)
            ])
        ])
    ];
};
