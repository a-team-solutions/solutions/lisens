import { HElements } from "peryl/dist/hsml";
import { HView } from "peryl/dist/hsml-app";

export interface NotifState {
    title: string;
    apiKey: string;
}

export enum NotifAction {
    snackbar = "notif-snackbar",
    browser = "notif-browser",
    pushy = "notif-pushy"
}

export const notifState: NotifState = {
    title: "Notifications",
    apiKey: ""
};

export const notifView: HView<NotifState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
        ["h2", "Snackbar notification"],
        ["form.w3-container", { on: ["submit", NotifAction.snackbar] },
            [
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Snackbar message"
                        }]
                    ]]
                ]],
                ["button.w3-btn.w3-blue", "Send"]
            ]
        ],
        ["h2", "Browser notification"],
        ["form.w3-container", { on: ["submit", NotifAction.browser] },
            [
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Browser message"
                        }]
                    ]]
                ]],
                ["button.w3-btn.w3-blue", "Send"]
            ]
        ],
        ["h2", "Push notification"],
        ["form.w3-container", { on: ["submit", NotifAction.pushy] },
            [
                ["p", [
                    ["label", [
                        "Pushy API key (pushy.me)",
                        ["input.w3-input", {
                            type: "text",
                            name: "apiKey",
                            value: state.apiKey
                        }]
                    ]],
                ]],
                ["p", [
                    ["label", [
                        "Title",
                        ["input.w3-input", {
                            type: "text",
                            name: "title",
                            value: "Pushy title"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Pushy message"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "URL",
                        ["input.w3-input", {
                            type: "text",
                            name: "url",
                            value: ""
                        }]
                    ]]
                ]],
                ["p", [
                    ["button.w3-btn.w3-blue", "Send"]
                ]]
            ]
        ]
    ]]
];
