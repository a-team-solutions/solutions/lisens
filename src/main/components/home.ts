import { HElements, HElement } from "peryl/dist/hsml";
import { HView } from "peryl/dist/hsml-app";
import { h } from "peryl/dist/hsml-h";
import {
    LicensedDependencyTree,
    LicensedDependency,
} from "../../../server/src/model/dependency";

export interface HomeState {
    title: string;
    text: string;
    dependencyTree?: LicensedDependencyTree;
}

export const homeState: HomeState = {
    title: "Home",
    text: ""
};

function dependencyView(dependency: LicensedDependency, deep: number): HElement {
    return h("div", [
        h("a", [
            h("div.columns", [
                h("div.column", [
                    h("span.title.is-5", dependency.name),
                ]),
                h("div.column", [
                    h("div.field.is-grouped.is-grouped-multiline.is-pulled-right", [
                        h("div.control", [
                            h("div.tag.is-link", dependency.version)
                        ]),
                        h("div.control", [
                            h("div.tag.is-warning", dependency.licenses.map(license => license.licenseId).join(", "))
                        ]),
                    ]),
                ])
            ])
        ]),
        dependency.dependencies ? h("ul", Object.entries(dependency.dependencies)
            .map(([name, depData]) => h("li", [dependencyView(depData, deep + 1)]))
        ) : null
    ]);
}

export const homeView: HView<HomeState> = (state): HElements => {
    return [
        h("div.box", [
            h("div.columns", [
                h("div.column", [
                    h("span.has-text-weight-semibold", "Package name"),
                ]),
                h("div.column", [
                    h("div.field.is-grouped.is-grouped-multiline.is-pulled-right", [
                        h("div.control", [
                            h("span.has-text-weight-semibold", "Version")
                        ]),
                        h("div.control", [
                            h("span.has-text-weight-semibold", "License")
                        ]),
                    ]),
                ])
            ]),
            h("div.menu.dependency-tree", [
                state.dependencyTree ? h("ul.menu-list.parent", Object.entries(state.dependencyTree.dependencies)
                    .map(([name, depData]) => h("li", [dependencyView(depData, 1)]))
                ) : null
            ])
        ])
    ];
};
