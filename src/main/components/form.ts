import { HElements, HElement, hjoin } from "peryl/dist/hsml";
import { HView } from "peryl/dist/hsml-app";
import { FormValidatorData } from "peryl/dist/validators";

export interface FormData {
    name: string;
    age: number;
    married: boolean;
    gender: string;
    sport: string;
}

export interface FormState {
    title: string;
    data: FormData;
    genders: {
        label: string;
        value: string;
    }[];
    sports: string[];
    validatorData?: FormValidatorData<FormData>;
}

export const formState: FormState = {
    title: "Form",
    data: {
        name: "Ema",
        age: 33,
        married: false,
        gender: "female",
        sport: "gymnastics"
    },
    genders: [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" }
    ],
    sports: ["football", "gymnastics"]
};

export enum FormAction {
    change = "form-change",
    submit = "form-submit"
}

export const formView: HView<FormState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
        ["form.w3-container",
            {
                on: [
                    ["change", FormAction.change],
                    ["submit", FormAction.submit]
                ]
            },
            [
                ["p", [
                    ["label", ["Name",
                        ["input.w3-input", {
                            type: "text",
                            name: "name",
                            value: state.validatorData.str.name
                        }]
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.name]],
                ["p", [
                    ["label", ["Age",
                        ["input.w3-input", {
                            type: "number",
                            name: "age",
                            value: state.validatorData.str.age
                        }]
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.age]],
                ["p", [
                    ["label", [
                        ["input.w3-check", {
                            type: "checkbox",
                            name: "married",
                            checked: state.validatorData.str.married
                        }],
                        " Married"
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.married]],
                ["p",
                    hjoin(
                        state.genders.map<HElement>(gender => (
                            ["label", [
                                ["input.w3-radio", {
                                    type: "radio",
                                    name: "gender",
                                    value: gender.value,
                                    checked: state.validatorData.str.gender === gender.value
                                }],
                                " ", gender.label
                            ]]
                        )),
                        ["br"]
                    )
                ],
                ["p.w3-text-red", [state.validatorData.err.gender]],
                ["p", [
                    ["select.w3-select", { name: "sport" },
                        [
                            ["option",
                                { value: "", disabled: true, selected: true },
                                "Sport"
                            ],
                            ...state.sports.map<HElement>(sport => (
                                ["option",
                                    {
                                        value: sport,
                                        selected: sport === state.validatorData.str.sport
                                    },
                                    sport
                                ])
                            )
                        ]
                    ]
                ]],
                ["p.w3-text-red", state.validatorData.err.sport],
                ["button.w3-btn.w3-blue", "Submit"]
            ]
        ]
    ]]
];
