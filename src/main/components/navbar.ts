import { HElement } from "peryl/dist/hsml";
import { HView1 } from "peryl/dist/hsml-app";
import { AppShellAction } from "./appshell";
import { h } from "peryl/dist/hsml-h";

export interface NavbarState {
    title: string;
    menu: {
        url: string;
        label: string;
    }[];
    menuActive: string;
    isMobileActive: boolean;
}

export const navbarState: NavbarState = {
    title: "Lisens",
    menu: [
        { url: "#", label: "Home" },
        { url: "#notif", label: "Notif" },
        { url: "#form", label: "Form" },
    ],
    menuActive: "active",
    isMobileActive: false
};

export const navbarView: HView1<NavbarState> = (state): HElement =>
    h("nav.navbar.is-warning", {
        "role": "navigation",
        "aria-label": "main navigation"
    }, [
        h("div.container", [
            h("div.navbar-brand", [
                h("a.navbar-item", {
                    href: "#"
                }, [
                    h("img", {
                        src: "images/lisens.png",
                        styles: { "margin-right": "25px" }
                    }),
                    h("h1.title.tt", state.title)
                ]),
                // Mobile burger
                h("a.navbar-burger burger", {
                    "role": "button",
                    "aria-label": "menu",
                    "aria-expanded": "false",
                    "data-target": "navbarLisens",
                    on: ["click", AppShellAction.mobileMenu]
                }, [
                    h("span", { "aria-hidden": "true" }),
                    h("span", { "aria-hidden": "true" }),
                    h("span", { "aria-hidden": "true" })
                ]),
            ]),
            // Menu
            h("div#navbarLisens.navbar-menu", {
                classes: [["is-active", state.isMobileActive]],
            },
                [
                    h("div.navbar-start", state.menu.map<HElement>(m => (
                        ["a.navbar-item", {
                            href: m.url,
                            on: ["click", AppShellAction.menu]
                        }, m.label]
                    )
                    ))
                ])
        ]),


    ]
    );

