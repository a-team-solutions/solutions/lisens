import { HApp, HAppAction, HDispatcher, HContext } from "peryl/dist/hsml-app";
import { appShellState, appShellView, AppShellAction, AppShellState } from "./components/appshell";
import { Hash } from "peryl/dist/hash";
import * as http from "peryl/dist/http";
import { NotifAction } from "./components/notif";
import { FormAction, FormData, FormState } from "./components/form";
import { FormValidator, StringValidator, NumberValidator, SelectValidator, BooleanValidator } from "peryl/dist/validators";
import { swInit, showNotification } from "./lib/service-worker-lib";

function snacbarNotif(app: HContext<AppShellState>, message: string): void {
    app.state.snackbar = message;
    app.update();
    setTimeout(
        () => {
            app.state.snackbar = undefined;
            app.update();
        },
        3e3);
}

class Pushy {

    readonly appId: string;
    readonly deviceToken: string;

    constructor(appId: string) {
        this.appId = appId;
        (window as any).Pushy.register({ appId })
            .then((deviceToken: string) => {
                console.log("Pushy device token: " + deviceToken);
                (this.deviceToken as any) = deviceToken;
                // Send the token to your backend server via an HTTP GET request
                // fetch("https://your.api.hostname/register/device?token=" + deviceToken);
                // Succeeded, optionally do something to alert the user
            })
            .catch((err: Error) => {
                console.error(err);
            });
    }

    push(apiKey: string, title: string, message: string, url?: string): void {
        http.post("https://api.pushy.me/push", { api_key: apiKey })
            .onError(e => console.error("pushy send error", e))
            .onResponse(r => console.log("pushy send response", r.getJson()))
            .send({
                to: this.deviceToken, // Your device token
                data: {
                    title, // Notification title
                    message, // Notification body text
                    url: url || location.href // Opens when clicked
                    // image: "https://example.com/image.png" // Optional image
                }
            });
    }

}


function browserNotif(message: string) {
    showNotification(message, {
        body: "Notification body",
        // icon: "images/icon.png",
        // badge: "images/badge.png",
        vibrate: [100, 50, 100],
        data: {
            onAction: {
                // "": location.href,
                like: "http://bbb.sk",
                reply: "http://ccc.sk"
            }
        },
        actions: [
            { action: "like", title: "Like" },
            { action: "reply", title: "Reply" }
        ]
    });
}

function validator(formState: FormState): FormValidator {
    return new FormValidator<FormData>()
        .addValidator("name", new StringValidator(
            {
                required: true,
                min: 3,
                max: 5
            },
            {
                required: "Required {{min}} {{max}} {{regexp}}",
                invalid_format: "Invalid format {{regexp}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("age", new NumberValidator(
            {
                required: true,
                min: 1,
                max: 100,
            },
            {
                required: "Required {{min}} {{max}} {{locale}} {{format}}",
                invalid_format: "Invalid format {{num}} {{locale}} {{format}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("gender", new SelectValidator(
            {
                required: true,
                options: formState.genders.map(g => g.value)
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .addValidator("married", new BooleanValidator(
            {
                required: true
            },
            {
                required: "Required",
                invalid_value: "Invalid value {{value}}"
            }))
        .addValidator("sport", new SelectValidator(
            {
                required: true,
                options: formState.sports
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .format(formState.data);
}

let formValidator: FormValidator<FormData>;

const pushyApiKey = "pushyApiKey";

const dispatcher: HDispatcher<AppShellState> = (app, action) => {
    console.log("app action", action);

    switch (action.type) {

        case HAppAction._init:
            app.state.contents.notif.apiKey = localStorage.getItem(pushyApiKey) || "";
            formValidator = validator(app.state.contents.form);
            app.state.contents.form.validatorData = formValidator.data();

            http.get("output.json")
                .onError(e => console.error("error", e))
                .onResponse(r => {
                    console.log(r.getJson());
                    app.state.contents.home.dependencyTree = r.getJson();
                    app.update();
                }).send();
            app.update();
            break;

        case HAppAction._mount:
        case HAppAction._umount:
            break;

        case AppShellAction.menu:
            app.state.menu = action.data === null ? !app.state.menu : action.data;
            app.update();
            break;

        case AppShellAction.mobileMenu:
            app.state.navbar.isMobileActive = action.data === null ? !app.state.navbar.isMobileActive : action.data;
            app.update();
            break;

        case NotifAction.snackbar:
            snacbarNotif(app, action.data.message);
            break;

        case NotifAction.browser:
            browserNotif(action.data.message);
            break;

        case NotifAction.pushy:
            action.data.apiKey &&
                localStorage.setItem(pushyApiKey, action.data.apiKey);
            pushy.push(
                action.data.apiKey,
                action.data.title,
                action.data.message,
                action.data.url);
            break;

        case FormAction.change:
            formValidator.validate({
                ...app.state.contents.form.validatorData.str,
                ...action.data
            });
            console.log("obj:", JSON.stringify(formValidator.data(), null, 4));
            app.state.contents.form.validatorData = formValidator.data();
            app.update();
            break;

        case FormAction.submit:
            action.event.preventDefault();
            if (formValidator.valid) {
                app.state.contents.form.data = formValidator.obj;
                const formData = JSON.stringify(app.state.contents.form.data, null, 4);
                console.dir(formData);
                alert(`Form submit: \n${formData}`);
            }
            break;

        default:
            console.warn("unhandled:", action);
    }
};

// HApp.debug = true;

const app = new HApp(appShellState, appShellView, dispatcher).mount();
(self as any).app = app;

new Hash<string>()
    .onChange(module => {
        console.log("hash: " + JSON.stringify(module));
        app.state.sidebar.menuActive = module;
        app.state.content = module;
        app.state.menu = false;
        app.update();
    })
    .listen();

swInit();

const pushy = new Pushy("5dca8013574cb62d339c084f");

(self as any).pushy = pushy;
