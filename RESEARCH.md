# Research

## 1. Comparing input data

### 1.1. package.json

+ easy to get (read only `package.json`)
+ small
- no transitive dependencies
- not equal to `node_modules/`

### 1.2. package-lock.json

+ easy to get (read only `package-lock.json`)
+ transitive dependencies
- not equal to `node_modules/`

### 1.3. npm list

+ transitive dependencies
+ equal to `node_modules/`
- slow to get (will read whole `node_modules/`)

### 1.4. npm-license-checker

+ licenses included
+ equal to `node_modules/`
- slow to get (will read whole `node_modules/`)
- no transitive dependencies

## 2. Getting license

### 2.1. Checking package.json

Most of time, license is included in package's `package.json` file, but it could
have several different forms.

- license as `string`
    - in this case, it could be exact license
    - or license with typo
    - or `SEE LICENSE IN <filename`
- license as `object` of `{ type: string; url: string; }`
    - license should be included into `type` property
    - otherwise is good to visit url
- license as `array` of objects `Array<{ type: string; url: string; }>`
    - should be treated in same way as previous one but as list licenses

**TIPS:** always try to parse license with `spdx-expression-parse` package

### 2.2. Reading LICENSE, README or COPYING files

Sometimes, license could be found in text files like `LICENSE.md`, `LICENSE`, `COPYING` or even `README`. Here is precedence order:

- license file (e.g. `license.md`, `LICENCE`)
- license file with suffix (e.g. `LICENSE-MIT`)
- `COPYING` file
- `README`
