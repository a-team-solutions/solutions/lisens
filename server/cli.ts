import * as yargs from "yargs";
import * as path from "path";
import { writeFileSync } from "fs";
import { Logic } from "./src/logic/logic";
import { LicenseLogic } from "./src/logic/licenselogic";
import { DiskLicenseCache } from "./src/module/disk-license-cache";
import { UNPKGApi } from "./src/module/unpkg-api";

const argv = yargs
    .option("npmlistPath", {
        type: "string",
        description: "path npmlist.json",
        demandOption: true
    })
    .option("output", {
        type: "string",
        description: "output path",
        demandOption: true
    })
    .argv;

async function main() {
    const npmlistPath = path.resolve(process.cwd(), argv.npmlistPath);
    const npmlist = require(npmlistPath); // TODO: validate input against schema
    // TODO: modularize cached path
    const cachedJSONPath = path.resolve(process.cwd(), argv.output);

    const diskCache = new DiskLicenseCache(cachedJSONPath);
    const unpkgApi = new UNPKGApi();

    const licenseLogic = new LicenseLogic(diskCache, unpkgApi);
    const appLogic = new Logic({} as any, {} as any, {} as any, licenseLogic);

    const licensedTree = await appLogic.license().resolveLicenses(npmlist);
    // const flatten = await appLogic.license().flattenLicenses(npmlist);

    if (argv.output) {
        const outputPath = path.resolve(argv.output);
        writeFileSync(outputPath, JSON.stringify(licensedTree, null, 2), { encoding: "utf-8" });
        console.log(`Licensed tree written into ${argv.output}`);
    } else {
        console.log(JSON.stringify(licensedTree, null, 2));
    }
}

main()
    .then(() => process.exit(0))
    .catch((err) => {
        throw err;
    });

