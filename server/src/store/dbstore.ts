import * as log4js from "log4js";
import { basename, extname } from "path";
import { Db, Collection, MongoClient, MongoClientOptions } from "mongodb";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export interface DbStoreConf {
    uri: string;
    options: MongoClientOptions;
}

export class DbStore {

    private _client: MongoClient;
    private _db: Db;
    private _conf: DbStoreConf;

    // public ObjectID: (id: string) => any;
    // public ObjectID: ObjectID;

    constructor(conf?: DbStoreConf) {
        this._conf = conf;
    }

    async conn(): Promise<Db> {
        if (this._db) {
            return Promise.resolve(this._db);
        }
        const { uri, options } = this._conf;
        try {
            this._client = await MongoClient.connect(uri, options);
            this._db = this._client.db();
            this._db.on("close", async () => {
                log.warn("DbStore close");
                await this._client.close();
                this._db = undefined;
            });
            this._db.on("reconnect", () => {
                log.warn("DbStore reconnect");
            });
            this._db.on("timeout", () => {
                log.warn("DbStore timeout");
            });
            this._db.on("fullsetup", () => {
                log.warn("DbStore fullsetup");
            });
            // this.ObjectID = ObjectID;
            log.debug("DB connected");
        } catch (err) {
            log.warn("DbStore error", err);
            await this._client.close();
            this._db = undefined;
            throw err;
        }
        return this._db;
    }

    close(): Promise<void> {
        return this._client ? this._client.close(true) : Promise.resolve();
    }

    async collectionNames(): Promise<string[]> {
        const db = await this.conn();
        const collections = await db.collections();
        return collections.map(c => c.collectionName);
    }

    async collection<U>(name: string): Promise<Collection<U>> {
        const db = await this.conn();
        const collection = db.collection<U>(name);
        return Promise.resolve(collection);
    }

    async collectionDrop(name: string): Promise<boolean> {
        const col = await this.collection(name);
        return await col.drop();
    }

    async dump<U>(collectionName: string): Promise<U[]> {
        const db = await this.conn();
        return await db.collection(collectionName).find().toArray();
    }

    async dumpAll(): Promise<any> {
        const collections = await this.collectionNames();
        const data: any = {};
        await Promise.all(
            collections.map(async c => {
                const dump = await this.dump(c);
                data[c] = dump;
            }));
        return data;
    }

    async load<U>(collectionName: string, data: U[]): Promise<U[]> {
        data.forEach(d => delete (d as any)._id);
        const db = await this.conn();
        return await db.collection<U>(collectionName)
            .insertMany(data as any)
            .then((res: any) => res.ops);
    }

    async loadAll(data: { [k: string]: any[] }): Promise<{ [k: string]: any[] }> {
        const res: { [k: string]: any[] } = {};
        for (const k in data) {
            res[k] = await this.load(k, data[k]);
        }
        return res;
    }

    async deleteAll(collectionName: string): Promise<any> {
        const db = await this.conn();
        return await db.collection(collectionName)
            .deleteMany({})
            .then(res => res.deletedCount);
    }

}
