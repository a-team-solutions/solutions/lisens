import * as log4js from "log4js";
import { basename, extname } from "path";
import * as path from "path";
import * as express from "express";
import { generateRoutes, generateSwaggerSpec, RoutesConfig, SwaggerConfig } from "tsoa";
// import { authBasicOrJwt } from "./middleware/authBasicOrJwt";
import * as swaggerUi from "swagger-ui-express";
import * as cors from "cors";
import * as bodyParser from "body-parser";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export function api(app: express.Express) {
    (async () => {
        const swaggerOutputDirectory = "public/api";

        const conf = app.conf;

        const swaggerConfig: SwaggerConfig = {
            specVersion: 3,
            name: "Notification",
            description: "Notification service",
            schemes: conf.api.swagger.schemes,
            host: conf.api.swagger.host,
            basePath: conf.api.swagger.basePath,
            version: conf.api.swagger.version,
            tags: [
                {
                    name: "API",
                    description: " Notification API",
                    externalDocs: {
                        description: "Swagger",
                        url: "http://swagger.io"
                    }
                }
            ],
            securityDefinitions: {
                basic_auth: {
                    type: "http",
                    scheme: "basic"
                },
                jwt: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                } as any,
                api_key: {
                    type: "apiKey",
                    in: "query",
                    name: "api_key"
                },
                tsoa_auth: {
                    type: "oauth2",
                    flows: {
                        implicit: {
                            authorizationUrl: "http://swagger.io/api/oauth/dialog",
                            scopes: {
                                "write:pets": "modify things",
                                "read:pets": "read things"
                            }
                        }
                    }
                } as any
            },
            outputDirectory: `./${swaggerOutputDirectory}`,
            entryFile: "./src/app.ts",
            noImplicitAdditionalProperties: "silently-remove-extras"
        };

        const routesConfig: RoutesConfig = {
            basePath: conf.api.basePath,
            entryFile: "./src/app.ts",
            routesDir: "./src/api-generated",
            authenticationModule: "./src/api/authentication.ts",
            controllerPathGlobs: ["./src/api/**/*.controller.ts"]
        };

        await generateRoutes(routesConfig, swaggerConfig);
        await generateSwaggerSpec(swaggerConfig, routesConfig);

        const apiBasePath = conf.api.basePath;

        app.use(apiBasePath, cors());
        app.use(apiBasePath, bodyParser.json());
        app.use(apiBasePath, bodyParser.urlencoded({ extended: true }));
        // app.use(apiBasePath, authBasicOrJwt(conf.auth));

        const routes = require(path.join(__dirname, "/api-generated/routes"));
        routes.RegisterRoutes(app);

        app.use(apiBasePath, (err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (req.is("json") !== false) {
                res.status((err as any).status || 500).json({ error: err.message || err });
            } else {
                next(err);
            }
        });

        try {
            const swaggerPath = path.join(__dirname, "../", swaggerOutputDirectory, "swagger.json");
            const swaggerDoc = require(swaggerPath);
            app.use(apiBasePath, swaggerUi.serve, swaggerUi.setup(swaggerDoc));
        } catch (err) {
            log.error("Unable to load swagger.json", err);
        }
    })();
}
