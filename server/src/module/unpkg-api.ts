import * as request from "request";
import { PackageJSON } from "../model/package-json";

type PackageNode = {
    path: string;
    type: "file" | "folder";
    contentType: string;
    integrity: string;
    lastModified: string;
    size: number;
}

type PackageTree = {
    path: string;
    type: string;
    files: PackageNode[];
};

export class UNPKGApi {

    /**
     * @TODO: Do research about unspecified pkgVersion
     */
    async fetchPackageJSON(pkgName: string, pkgVersion?: string): Promise<{ url: string; body: PackageJSON }> {
        const url = pkgVersion
            ? `https://unpkg.com/${pkgName}@${pkgVersion}/package.json`
            : `https://unpkg.com/${pkgName}/package.json`
        return new Promise<any>((resolve, reject) => {
            request.get(url, {}, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve({
                    url,
                    body: JSON.parse(res.body)
                });
            });
        });
    }

    async fetchPackageTree(pkgName: string, pkgVersion: string): Promise<{ url: string; body: PackageTree; }> {
        const url = `https://unpkg.com/${pkgName}@${pkgVersion}/?meta`;
        return new Promise<any>((resolve, reject) => {
            request.get(url, {}, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve({
                    url,
                    body: JSON.parse(res.body)
                });
            });
        });
    }

}
