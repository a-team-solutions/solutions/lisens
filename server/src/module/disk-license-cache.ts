import { readFileSync, existsSync, writeFileSync } from "fs";
import { ILicenseCache } from "../model/license-cache";
import { CachedPkgLicenses, CachedPkgLicense } from "../model/cached-pkg-license";
import { License } from "../model/License";

export class DiskLicenseCache implements ILicenseCache {

    private _cacheFilePath: string;
    private _memCache: CachedPkgLicenses;

    constructor(jsonPath: string) {
        this._cacheFilePath = jsonPath;
        if (!existsSync(jsonPath)) {
            console.log(`[DiskLicenseCache] Cached json doesn't exists, creating one in "${jsonPath}" `);
            writeFileSync(jsonPath, JSON.stringify({}), { encoding: "utf-8" });
            this._memCache = { };
        } else {
            console.log(`[DiskLicenseCache] Loading cached json from "${jsonPath}"`);
            const cacheString = readFileSync(jsonPath, { encoding: "utf-8" });
            try {
                const parsedCache = JSON.parse(cacheString);
                this._memCache = parsedCache;
            } catch(err) {
                throw new Error(`[DiskLicenseCache] Cannot parse cached json in "${jsonPath}"`);
            }
        }
    }

    private _writeToDisk(): void {
        const cachedString = JSON.stringify(this._memCache, null, 2);
        console.log(`[DiskLicenseCache] Writing changes to disk`);
        writeFileSync(this._cacheFilePath, cachedString, { encoding: "utf-8" });
    }

    async getLicense(name: string, version: string): Promise<CachedPkgLicense | undefined> {
        const cachedName = `${name}@${version}`;
        if (cachedName in this._memCache) {
            return this._memCache[cachedName];
        }
        return undefined;
    }

    async setLicense(name: string, version: string, data: { licenses: License[]; category: string;resolved: { type: "package.license"; source: string; url: string; }}): Promise<CachedPkgLicense> {
        const cachedName = `${name}@${version}`;
        //is license cached?
        if (cachedName in this._memCache) {
            const cachedLicense: CachedPkgLicense = {
                ...this._memCache[cachedName],
                updatedAt: Date.now(),
                licenses: data.licenses,
                resolved: data.resolved
            };
            this._memCache[cachedName] = cachedLicense;
            this._writeToDisk();
            return cachedLicense;
        } else {
            const cachedLicensed: CachedPkgLicense = {
                createdAt: Date.now(),
                licenses: data.licenses,
                resolved: data.resolved
            };
            this._memCache[cachedName] = cachedLicensed;
            this._writeToDisk();
            return cachedLicensed;
        }
    }
}
