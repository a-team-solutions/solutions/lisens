export interface LockDependency {
    /**
     * This is a specifier that uniquely identifies this package and should be usable in fetching a new copy of it.
     */
    version: string;
    /**
     * This is a Standard Subresource Integrity for this resource.
     *
     * For bundled dependencies this is not included, regardless of source.
     * For registry sources, this is the integrity that the registry provided, or if one wasn’t provided the SHA1 in shasum.
     * For git sources this is the specific commit hash we cloned from.
     * For remote tarball sources this is an integrity based on a SHA512 of the file.
     * For local tarball sources: This is an integrity field based on the SHA512 of the file.
     */
    integrity: string;
    /**
     * For bundled dependencies this is not included, regardless of source.
     * For registry sources this is path of the tarball relative to the registry URL.
     * If the tarball URL isn’t on the same server as the registry URL then this is a complete URL.
     */
    resolved: string;
    /**
     * If true, this is the bundled dependency and will be installed by the parent module.
     * When installing, this module will be extracted from the parent module during the extract phase,
     * not installed as a separate dependency.
     */
    bundled?: boolean;
    /**
     * If true then this dependency is either a development dependency ONLY of the top level module or a transitive dependency of one.
     * This is false for dependencies that are both a development dependency of the top level and a transitive dependency
     * of a non-development dependency of the top level.
     */
    dev?: boolean;
    /**
     * If true then this dependency is either an optional dependency ONLY of the top level module or a transitive dependency of one.
     * This is false for dependencies that are both an optional dependency of the top level and
     * a transitive dependency of a non-optional dependency of the top level.
     *
     * All optional dependencies should be included even if they’re uninstallable on the current platform.
     */
    optional?: boolean;
    /**
     * This is a mapping of module name to version. This is a list of everything this module requires,
     * regardless of where it will be installed. The version should match via normal matching rules a dependency
     * either in our dependencies or in a level higher than us.
     */
    requires?: {
        [depName: string]: string; // semver;
    };
    /**
     * The dependencies of this dependency, exactly as at the top level.
     */
    dependencies?: {
        [depName: string]: LockDependency;
    }
}

export interface PackageLock {
    /**
     * The name of the package this is a package-lock for. This must match what’s in package.json.
     */
    name: string;
    /**
     * The version of the package this is a package-lock for. This must match what’s in package.json.
     */
    version: string;
    /**
     * An integer version, starting at 1 with the version number of this document whose semantics were used
     * when generating this package-lock.json.
     */
    lockfileVersion: number;
    /**
     * This is a subresource integrity value created from the package.json. No preprocessing of the package.json should be done.
     * Subresource integrity strings can be produced by modules like ssri.
     */
    packageIntegrity?: any;
    /**
     * Indicates that the install was done with the environment variable NODE_PRESERVE_SYMLINKS enabled.
     * The installer should insist that the value of this property match that environment variable.
     */
    preserveSymlinks: any;
    dependencies?: {
        [depName: string]: LockDependency;
    }
}
