import { ILicenseCache } from "../model/license-cache";
import { CachedPkgLicense } from "../model/cached-pkg-license";
import { License } from "../model/License";

export class MemLicenseCache implements ILicenseCache {

    private _memCache: { [name: string]: CachedPkgLicense; } = {};

    async getLicense(name: string, version: string): Promise<CachedPkgLicense | undefined> {
        const cachedName = `${name}@${version}`;
        if (cachedName in this._memCache) {
            return this._memCache[cachedName];
        }
        return undefined;
    }

    async setLicense(name: string, version: string, data: { licenses: License[]; category: string; resolved: { type: "package.license"; source: string; url: string; }}): Promise<CachedPkgLicense> {
        const cachedName = `${name}@${version}`;
        if (cachedName in this._memCache) {
            const cachedLicense = {
                ...this._memCache[cachedName],
                updatedAt: Date.now(),
                licenses: data.licenses,
                resolved: data.resolved
            };
            this._memCache[cachedName] = cachedLicense;
            return cachedLicense;
        } else {
            const cachedLicensed: CachedPkgLicense = {
                createdAt: Date.now(),
                licenses: data.licenses,
                resolved: data.resolved
            };
            this._memCache[cachedName] = cachedLicensed;
            return cachedLicensed;
        }
    }

}
