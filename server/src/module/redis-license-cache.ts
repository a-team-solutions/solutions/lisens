import * as redis from "redis";
import { ILicenseCache } from "../model/license-cache";
import { CachedPkgLicense } from "../model/cached-pkg-license";
import { License } from "../model/License";

export class RedisLicenseCache implements ILicenseCache {

    private _redisClient: redis.RedisClient;

    constructor(options: redis.ClientOpts) {
        this._redisClient = redis.createClient(options);
    }

    async getLicense(name: string, version: string): Promise<CachedPkgLicense | undefined> {
        const cachedName = `${name}@${version}`;
        return new Promise((resolve, reject) => {
            this._redisClient.get(cachedName, (err, res) => {
                if (err) {
                    return reject(err);
                }
                if (res) {
                    return resolve(JSON.parse(res));
                }
                return undefined;
            });
        });
    }

    async setLicense(name: string, version: string, data: { licenses: License[]; category: string; resolved: { type: "package.license"; source: string; url: string; }}): Promise<CachedPkgLicense> {
        const cachedName = `${name}@${version}`;
        const license = await this.getLicense(name, version);
        return new Promise((resolve, reject) => {
            if (license) {
                const cachedLicense: CachedPkgLicense = {
                    ...license,
                    updatedAt: Date.now(),
                    licenses: data.licenses,
                    resolved: data.resolved
                }
                this._redisClient.set(cachedName, JSON.stringify(cachedLicense), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(cachedLicense);
                });
            } else {
                const cachedLicense: CachedPkgLicense = {
                    createdAt: Date.now(),
                    licenses: data.licenses,
                    resolved: data.resolved
                };
                this._redisClient.set(cachedName, JSON.stringify(cachedLicense), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(cachedLicense);
                });
            }
        });
    }

}
