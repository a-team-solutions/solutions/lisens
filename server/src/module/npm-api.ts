import * as request from "request";

export class NPMApi {

    // TODO: check
    async fetchAuditData(packageLock: {}) {
        return new Promise<any>((resolve, reject) => {
            request.post("https://registry.npmjs.org/-/npm/v1/security/audits", {
                json: true,
                port: 443,
                body: packageLock
            }, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve({});
            });
        });
    }

}
