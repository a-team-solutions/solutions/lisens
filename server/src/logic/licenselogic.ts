import { ILicenseCache } from "../model/license-cache";
import { NPMList, NPMListDependency } from "../model/npmlist";
import { resolveLicense, UNKNOWN_LICENSE } from "../utils/resolve-license";
import { LicensedDependencyTree, LicensedDependency } from "../model/dependency";
import { UNPKGApi } from "../module/unpkg-api";
import { CachedPkgLicense } from "../model/cached-pkg-license";
import { License } from "../model/License";

export class LicenseLogic {

    private _cache: ILicenseCache;
    private _unpkgApi: UNPKGApi;

    constructor(cache: ILicenseCache, unpkgApi: UNPKGApi) {
        this._cache = cache;
        this._unpkgApi = unpkgApi;
    }

    private async _resolveLicenseForDependency(pkgName: string, pkgVersion: string): Promise<CachedPkgLicense> {
        const cachedLicense = await this._cache.getLicense(pkgName, pkgVersion);
        if (cachedLicense) {
            console.log(`[LicenseLogic] Using cache for ${pkgName}@${pkgVersion}`);
            return cachedLicense;
        }
        console.log(`[LicenseLogic] Fetching package.json for ${pkgName}@${pkgVersion}`);
        const response = await this._unpkgApi.fetchPackageJSON(pkgName, pkgVersion);
        const pkgJSON = response.body;
        if (pkgJSON.license) {
            // TODO: spdx sometimes returns array, ensure that every license is array of string
            if (Array.isArray(pkgJSON.license)) {
                const licenses: License[] = pkgJSON.licenses.map<License>((license) => resolveLicense(license.type));
                const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                    licenses: licenses,
                    category: "",
                    resolved: {
                        type: "package.license",
                        source: JSON.stringify(pkgJSON.license),
                        url: response.url
                    }
                });
                return cachedLicense;
            } else if (typeof pkgJSON.license === "string") {
                // TODO: string license sometimes contains "SEE LICENSE FILE <filename>", then you need to follow link
                try {
                    const license = resolveLicense(pkgJSON.license);
                    const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                        licenses: [license],
                        category: "",
                        resolved: {
                            type: "package.license",
                            source: pkgJSON.license,
                            url: response.url
                        }
                    });
                    return cachedLicense;
                } catch {
                    const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                        licenses: [],
                        category: "",
                        resolved: {
                            type: "package.license",
                            source: pkgJSON.license,
                            url: response.url
                        }
                    });
                    return cachedLicense;
                }
            } else if (pkgJSON.license.type) {
                try {
                    const license = resolveLicense(pkgJSON.license.type);
                    const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                        licenses: [license],
                        category: "",
                        resolved: {
                            type: "package.license",
                            source: JSON.stringify(pkgJSON.license),
                            url: response.url
                        }
                    });
                    return cachedLicense;
                } catch {
                    const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                        licenses: [UNKNOWN_LICENSE],
                        category: "",
                        resolved: {
                            type: "package.license",
                            source: JSON.stringify(pkgJSON.license),
                            url: response.url
                        }
                    });
                    return cachedLicense;
                }
            } else {
                const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                    licenses: [UNKNOWN_LICENSE],
                    category: "",
                    resolved: {
                        type: "package.license",
                        source: JSON.stringify(pkgJSON.license),
                        url: response.url
                    }
                });
                return cachedLicense;
            }
        // `licenses` property convention is no longer supported by NPM, but still a lot of old packages are using it
        } else if (pkgJSON.licenses) {
            if (Array.isArray(pkgJSON.licenses)) {
                const licenses: License[] = pkgJSON.licenses.map((license) => resolveLicense(license.type));
                const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                    licenses,
                    category: "",
                    resolved: {
                        type: "package.licenses",
                        source: JSON.stringify(pkgJSON.license),
                        url: response.url
                    }
                });
                return cachedLicense;
            } else {
                const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                    licenses: [UNKNOWN_LICENSE],
                    category: "",
                    resolved: {
                        type: "package.licenses",
                        source: JSON.stringify(pkgJSON.license),
                        url: response.url
                    }
                });
                return cachedLicense;
            }
        } else {
            //Cannot find license in unpkg.io - package.json
            const cachedLicense = await this._cache.setLicense(pkgName, pkgVersion, {
                licenses: [UNKNOWN_LICENSE],
                category: "",
                resolved: {
                    type: "package.license",
                    source: JSON.stringify(pkgJSON.license),
                    url: response.url
                }
            });
            return cachedLicense;
        }
    }

    private async _getLicensesForDependencies(dependencies: { [name: string]: NPMListDependency }): Promise<{ [name: string]: LicensedDependency }> {
        const result: { [name: string]: LicensedDependency } = {};
        for (const [depName, depData] of Object.entries(dependencies)) {
            const license = await this._resolveLicenseForDependency(depName, depData.version);
            if (depData.dependencies) {
                result[depName] = {
                    name: depName,
                    resolved: depData.resolved,
                    version: depData.version,
                    licenses: license.licenses,
                    dependencies: await this._getLicensesForDependencies(depData.dependencies)
                };
            } else {
                result[depName] = {
                    name: depName,
                    resolved: depData.resolved,
                    version: depData.version,
                    licenses: license.licenses
                };
            }
        }
        return result;
    }

    private async _flattenDependencies(dependencies: { [name: string]: NPMListDependency }): Promise<LicensedDependency[]> {
        let resolved: LicensedDependency[] = [];
        for (const [name, data] of Object.entries(dependencies)) {
            const resolvedLicense = await this._resolveLicenseForDependency(name, data.version);
            resolved.push({
                name: name,
                resolved: data.resolved,
                version: data.version,
                licenses: resolvedLicense.licenses
            });
            if (data.dependencies) {
                resolved = resolved.concat(await this._flattenDependencies(data.dependencies));
            }
        }
        return resolved;
    }

    async flattenLicenses(npmlist: NPMList): Promise<LicensedDependency[]> {
        const licenses = await this._flattenDependencies(npmlist.dependencies);
        return licenses;
    }

    async resolveLicenses(npmlist: NPMList) {
        const dependencyTree: LicensedDependencyTree = {
            name: npmlist.name,
            version: npmlist.version,
            dependencies: await this._getLicensesForDependencies(npmlist.dependencies)
        };
        return dependencyTree;
    }

}
