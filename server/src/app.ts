import * as log4js from "log4js";
import { basename, extname } from "path";
import * as nconf from "nconf";
import * as path from "path";
import * as express from "express";
import * as helmet from "helmet";
import * as compression from "compression";
import * as session from "express-session";
import * as cookieParser from "cookie-parser";
import { users } from "./data/users";
import { confDefault, Conf } from "./conf";
import { objPaths } from "peryl/dist/objpaths";
import { rootRouter } from "./router/root";
import { sseRouter } from "./router/sse";
import { jserrRouter } from "./router/jserr";
import { demoRouter } from "./router/demo";
import { DbStore } from "./store/dbstore";
import { Mailer } from "./mail/mailer";
import { UserLogic } from "./logic/userlogic";
import { Logic } from "./logic/logic";
import { errorLog, errorJsonResponse, errorPage } from "./middleware/errorhandlers";
import { User } from "./model/user";
import { api } from "./api";
import { LicenseLogic } from "./logic/licenselogic";
import { MemLicenseCache } from "./module/mem-license-cache";
import { UNPKGApi } from "./module/unpkg-api";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

declare global {
    namespace Express {
        interface Application {
            conf: Conf;
            logic: Logic;
            user: User | undefined;
        }
    }
    namespace Express {
        interface Request {
            absBaseUrl: string;
            absUrl: string;
        }
    }
}

nconf
    .argv()
    .env({
        separator: "_",
        lowerCase: false,
        whitelist: objPaths(confDefault).map(c => c.join("_")),
        parseValues: true
    })
    .file(path.join(__dirname, "..", "conf.json"))
    .defaults(confDefault);

const conf = nconf.get() as Conf;

log4js.configure(conf.log4js);

log.info("NODE_ENV", process.env.NODE_ENV);
log.info("conf: %s", JSON.stringify(conf, null, 4));

const dbStore = new DbStore(conf.mongo);
const licenseLogic = new LicenseLogic(new MemLicenseCache(), new UNPKGApi());
const logic = new Logic(
    dbStore,
    new Mailer(conf.mailer),
    new UserLogic(dbStore),
    licenseLogic);

(async () => {
    await logic.user().removeAll();
    await Promise.all(users.map(async u => logic.user().insert(u)));

    // if (process.env.NODE_ENV === "production") {
    //     const data = await dbStore.dumpAll();
    //     const name = `backup-${new Date().toISOString()}`;
    //     const p = path.join(conf.dbdump, name);
    //     log.info("DB dump", p, " - ",
    //         Object.keys(data)
    //             .map(k => `${k}: ${data[k].length}`)
    //             .join(", "));
    //     fs.writeFileSync(p, JSON.stringify(data, null, 4));
    // } else {
    // }
})();

export const app = express();

app.conf = conf;
app.logic = logic;

app.use(log4js.connectLogger(log4js.getLogger("http"), { level: "auto" }));

app.use(express.json({ limit: '1mb'}));
app.use(helmet());

app.disable("x-powered-by");

app.use(compression());

app.set("trust proxy", 1); // trust first proxy
app.use( // https://github.com/andybiar/express-passport-redis-template/blob/master/app.js
    session({
        secret: "se-cu-re",
        name: "sessionId",
        resave: false,
        saveUninitialized: true
    })
);
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
// app.use(bodyParser.text());
app.use(cookieParser());

app.use((req, res, next) => {
    req.absBaseUrl = req.protocol + "://" + req.get("host");
    req.absUrl =  req.absBaseUrl + req.originalUrl;
    return next();
});

app.use("/", rootRouter);
app.use("/sse", sseRouter);
app.use("/jserr", jserrRouter);
app.use("/demo", demoRouter);

app.use(express.static(path.join(__dirname, "../public")));

app.use(errorLog);
app.use(errorJsonResponse);
app.use(errorPage);

api(app);
