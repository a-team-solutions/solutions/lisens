import * as log4js from "log4js";
import { basename, extname } from "path";
import * as basicAuth from "express-basic-auth";
import { Request, Response, NextFunction } from "express";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export const authBasic = (req: Request, res: Response, next: NextFunction) => {
    basicAuth({
        authorizeAsync: true,
        authorizer: (username, password, cb) => {
            log.debug(username, password);
            req.app.logic.user().findByLogin(username)
                .then(user => {
                    const auth = user && user.password === password;
                    log.debug("auth: ", username, password, user);
                    req.app.user = user;
                    cb(null, auth);
                })
                .catch(err => {
                    log.error(err);
                    cb(null, false);
                });
        },
        challenge: true,
        realm: "Imb4T3st4pp"
    })(req, res, next);
};
