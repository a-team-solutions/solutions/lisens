import * as path from "path";
import { SwaggerConfig } from "tsoa";
import { MongoClientOptions } from "mongodb";

const pkg = require(path.resolve(__dirname, "../package.json"));

export const confDefault = {
    log4js: {
        appenders: { out: { type: "stdout" } },
        categories: { default: { appenders: ["out"], level: "trace" } }
    },
    server: {
        host: "localhost",
        port: 8080
    },
    mailer: {
        name: "project",
        address: "neofinance@gmail.com",
        password: "projectsolutions"
    },
    mongo: {
        uri: "mongodb://root:projects@localhost:27017/admin",
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true
        } as MongoClientOptions
    },
    auth: {
        jwt: {
            secretOrPublicKey: "TEST",
            tokenSubjectClaim: "sub",
        },
        basic: {
            users: {
                user: "user",
                test: "test"
            } as { [user: string]: string },
            challenge: true,
            realm: "project"
        }
    },
    api: {
        basePath: "/api",
        swagger: {
            schemes: ["http"],
            host: "localhost:8080",
            basePath: "/api",
            version: pkg.version
        } as SwaggerConfig
    }
};

export type Conf = typeof confDefault;
