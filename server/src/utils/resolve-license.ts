const spdxExpressionParse = require("spdx-expression-parse");
// const spdxLicenses = require("../data/spdx-licenses.json");
import { License } from "../model/License";

function getUnambiguousCorrection(license: string) {
    if (license === 'Apache, Version 2.0') return 'Apache-2.0'
    if (license === 'Apache License, Version 2.0') return 'Apache-2.0'
    if (license === 'Apache License 2.0') return 'Apache-2.0'
    if (license === 'Apache 2.0') return 'Apache-2.0'
    if (license === 'Apache 2') return 'Apache-2.0'
    if (license === 'Apache2') return 'Apache-2.0'
    if (license === 'Apache v2') return 'Apache-2.0'
    if (license === 'MIT/X11') return 'MIT'
    if (license === 'LGPL 3') return 'LGPL-3.0'
    if (license === 'The MIT License') return 'MIT'
    if (license === 'MIT License') return 'MIT'
    if (license === 'MIT +no-false-attribs') return 'MITNFA'
    if (license === 'BSD-3') return 'BSD-3-Clause'
    if (license === 'BSD 3-Clause') return 'BSD-3-Clause'
    if (license === 'MPL 2.0') return 'MPL-2.0'
    return null
}

export const UNKNOWN_LICENSE: License = {
    name: "",
    licenseId: "UNKNOWN",
};

export function resolveLicense(unparsedLicesneStr: string): License {
    try {
        const parsedLicense = spdxExpressionParse(unparsedLicesneStr);
        // SPDX return object could have `license` or tree-like structure
        if (parsedLicense && parsedLicense.license) {
            return {
                name: "",
                licenseId: parsedLicense.license
            };
        } else {
            // TODO: Resolve tree-like structure
            return {
                name: "",
                licenseId: "TREE-LIKE"
            };
        }
    } catch(err) {
        return {
            name: "",
            licenseId: "CANNOT_PARSE"
        };
    }
}
