import { Tags, Route, Controller, Request, Security, Get, Post, Body     } from "tsoa";
import * as multer from "multer";
import * as express from "express";
import { NPMList } from "../model/npmlist";
import { LicensedDependencyTree } from "../model/dependency";

@Route("/license")
@Tags("License")
export class LicenseController extends Controller {

    private _handleFileUpload(request: express.Request): Promise<any> {
        const multerSingle = multer().single("uploadedFile");
        return new Promise((resolve, reject) => {
          multerSingle(request, undefined, async (error: any) => {
            if (error) {
              reject(error);
            }
            resolve();
          });
        });
      }

    @Post("/")
    async uploadNPMList(
        @Request() req: express.Request
    ): Promise<LicensedDependencyTree> {
        await this._handleFileUpload(req);
        const npmList: NPMList = (req as any).uploadedFile;
        console.log(npmList);
        return req.app.logic.license().resolveLicenses(npmList);
    }

}
