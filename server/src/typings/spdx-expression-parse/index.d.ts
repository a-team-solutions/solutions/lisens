declare module "spdx-expression-parse" {
    function parseSPDX(str: string): any;
    export default parseSPDX;
}
