import { Logic } from "../logic/logic";
import { User } from "../model/user";
import { Conf } from "../conf";

declare global {
    namespace Express {
        interface Application {
            conf: Conf;
            logic: Logic;
            user: User | undefined;
        }
    }
    namespace Express {
        interface Request {
            absBaseUrl: string;
            absUrl: string;
        }
    }
}
