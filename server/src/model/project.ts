interface LicenseAggregate {
    [license: string]: {
        /**
         * Without transitive packages
         */
        components: Array<{
            name: string;
            version: string;
        }>;
        spdxCode: string;
        category: string;
        /**
         * License URL
         */
        url: string;
    }
}

interface ProjectDependency {
    name: string;
    version: string;
    dependencies?: {
        [name: string]: ProjectDependency;
    };
}

export interface AppProject {
    name: string; // not a name from package.json
    version: string;
    description: string;
    dependencyTree: {
        [name: string]: ProjectDependency;
    };
}
