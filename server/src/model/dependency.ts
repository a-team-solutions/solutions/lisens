import { License } from "./License";

export interface LicensedDependency {
    name: string;
    version: string;
    resolved: string;
    licenses: License[];
    dependencies?: {
        [name: string]: LicensedDependency;
    };
}

export interface LicensedDependencyTree {
    name: string;
    version: string;
    dependencies: {
        [name: string]: LicensedDependency;
    };
}
