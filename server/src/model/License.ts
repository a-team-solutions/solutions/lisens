export interface License {
    /**
     * Full name of license
     * @example "MIT License"
     */
    name: string;
    /**
     * SPDX license id
     * @example "MIT"
     */
    licenseId: string;
}
