import { CachedPkgLicense } from "./cached-pkg-license";
import { License } from "../model/License";

export interface ILicenseCache {
    getLicense(name: string, version: string): Promise<CachedPkgLicense | undefined>;
    setLicense(name: string, version: string, data: { licenses: License[]; category: string; resolved: { type: string; source: string; url: string; }}): Promise<CachedPkgLicense>;
}
