import { License } from "./License";

export type CachedPkgLicense = {
    licenses: License[];
    resolved: {
        type: "package.license";
        url: string;
        source: string;
    } | {
        type: "package.licenses";
        url: string;
        source: string;
    };
    createdAt: number;
    updatedAt?: number;
};

export type CachedPkgLicenses = {
    // nameVersion == "peryl@1.0.0"
    [nameVersion: string]: CachedPkgLicense;
};
