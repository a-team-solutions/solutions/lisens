export interface NPMListDependency {
    version: string;
    from: string;
    resolved: string;
    dependencies?: {
        [name: string]: NPMListDependency;
    }
}

export interface NPMList {
    name: string; // package.json
    version: string; // package.json
    dependencies: {
        [name: string]: NPMListDependency;
    }
}

