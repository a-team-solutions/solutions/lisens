/**
 * { "license" : "BSD-3-Clause" }
 * { "license" : "(ISC OR GPL-3.0)" }
 * { "license" : "SEE LICENSE IN <filename>" }
 */
type LicensePlain = string;

/**
 * { "license" :
 *   { "type" : "ISC"
 *   , "url" : "https://opensource.org/licenses/ISC"
 *   }
 * }
 */
type LicenseObject = { type: string; url: string; };

/**
 * { "licenses" :
 *  [
 *    { "type": "MIT"
 *    , "url": "https://www.opensource.org/licenses/mit-license.php"
 *    }
 *  , { "type": "Apache-2.0"
 *    , "url": "https://opensource.org/licenses/apache2.0.php"
 *    }
 *  ]
 * }
 */
type PkgLicenses = Array<LicenseObject>;

export type PkgLicense = LicensePlain | LicenseObject;

export interface PackageJSON {
    license?: PkgLicense;
    licenses?: PkgLicenses;
}
